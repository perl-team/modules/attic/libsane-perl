libsane-perl (0.05-3) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jeffrey Ratcliffe from Uploaders. Thanks for your work!

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jul 2012 17:44:59 +0200

libsane-perl (0.05-2) unstable; urgency=low

  * Disable tests
    Closes: #626019 (FTBFS on various architectures (hanging tests?))

 -- Jeffrey Ratcliffe <jjr@debian.org>  Mon, 21 May 2012 21:52:44 +0200

libsane-perl (0.05-1) unstable; urgency=low

  * New upstream release

 -- Jeffrey Ratcliffe <jjr@debian.org>  Sun, 08 Apr 2012 12:10:02 +0200

libsane-perl (0.04-1) unstable; urgency=low

  * Team upload.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.04
    + Checks for opt->size=0 and zero-length data (LP: #930552).
  * Update debian/copyright file.
    Update Format to revision 174 of DEP5 proposal for machine-readable
    copyright file information.
    Update copyright years.
    Add explicit License stanzas for Artistic and GPL-1+ license.
    Refer to the GPL-1 license text in /usr/share/common-licenses/GPL-1.
  * Simplify debian/rules to a three line tiny makefile
  * Bump Debhelper compat level to 9.
    Adjust Build-Depends on debhelper to debhelper (>= 9).
  * Bump Standards-Version to 3.9.2
  * Convert to '3.0 (quilt)' source package format

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 15 Feb 2012 15:00:19 +0100

libsane-perl (0.03-1) unstable; urgency=low

  [ Jeffrey Ratcliffe ]
  * New upstream release
    Closes: #527695:
     (libsane-perl: FTBFS: Sane.xs:372: error: 'SANE_CAP_ALWAYS_SETTABLE'...)

  [ gregor herrmann ]
  * debian/control:
    - add Homepage field
    - add libtest-pod-perl to Build-Depends
    - remove base-files (>= 4.0.0) from Depends (base-files is essential, and
      even oldstable has version 4)

 -- Jeffrey Ratcliffe <Jeffrey.Ratcliffe@gmail.com>  Mon, 11 May 2009 21:30:04 +0200

libsane-perl (0.02-1) unstable; urgency=low

  * Initial release (Closes: #506405).

 -- Jeffrey Ratcliffe <Jeffrey.Ratcliffe@gmail.com>  Tue, 25 Nov 2008 20:31:30 +0100
